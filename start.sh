#!/usr/bin/env sh

export APP_NAME="Basic Crud"

export DIR_VIEWS="views"
export DIR_PARTIALS="views/partials"

export DB_DATABASE="database/basic_crud.db"
export DB_USERNAME=""
export DB_PASSWORD=""

./server
