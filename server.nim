import
  jester

import
  controllers/HomeController as home,
  controllers/TodoController as todo
  # controllers/BookController as book

routes:
  get "/": resp home.index()
  get "/error": resp home.error()

  get "/todo": resp todo.index()
  get "/todo/create": resp todo.create()
  post "/todo/create":
    try:
      todo.store(request)
      redirect("/todo")
    except:
      redirect("/todo/create")
  get "/todo/delete/@id":
    todo.destroy(@"id")
    redirect("/todo")

  # get "/books": resp book.index()

runForever()
