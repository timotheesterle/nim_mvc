# Package

version       = "0.1.0"
author        = "Timothée Sterle"
description   = "A basic crud in nim"
license       = "MIT"
srcDir        = "."
bin           = @["server"]



# Dependencies

requires "nim >= 1.0.6"
requires "jester"
requires "moustachu"
