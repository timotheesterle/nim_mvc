import os

putEnv("APP_NAME", "Basic Crud")

putEnv("DIR_VIEWS", "views")
putEnv("DIR_PARTIALS", "views/partials")

putEnv("DB_DATABASE", "database/basic_crud.db")
putEnv("DB_USERNAME", "")
putEnv("DB_PASSWORD", "")
