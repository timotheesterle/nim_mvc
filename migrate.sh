#!/usr/bin/env sh

PRG="sqlite3"
DATABASE="database/basic_crud.db"

die() {
  printf 'Erreur : %s.\n' "$1" >&2
  exit 1
}

main() {
  cd migrations || die "Le dossier de migrations n'existe pas"
  for file in *.sql; do
    "$PRG" "../$DATABASE" < "$file"
  done
}

main "$@"
