import
  os,
  moustachu,
  json

proc index*(): string =
  let view = readFile(getEnv("DIR_VIEWS") / "home.mustache")
  let fields = %* {
      "websiteName": getEnv("APP_NAME"),
      "pageTitle": "Home",
  }
  render(view, fields, getEnv("DIR_PARTIALS"))

proc error*(): string =
  let view = readFile(getEnv("DIR_VIEWS") / "error.mustache")
  let fields = %* {
      "websiteName": getEnv("APP_NAME"),
      "pageTitle": "Error",
  }
  render(view, fields, getEnv("DIR_PARTIALS"))
