import
  os,
  moustachu,
  jester,
  json,
  db_sqlite

proc index*(): string =
  let dbItems = %* []
  let query = sql"""
  SELECT id, text FROM todo_list
  """
  let db = open(getEnv("DB_DATABASE"), "", "", "")
  for item in db.instantRows(query):
    dbItems.add(%* {
      "itemId": item[0],
      "itemText": item[1],
    })
  db.close()

  let view = readFile(getEnv("DIR_VIEWS") / "todo/index.mustache")
  let fields = %* {
      "websiteName": getEnv("APP_NAME"),
      "pageTitle": "Todo-List",
      "item": dbItems,
  }
  render(view, fields, getEnv("DIR_PARTIALS"))

proc create*(): string =
  let view = readFile(getEnv("DIR_VIEWS") / "todo/create.mustache")
  let fields = %* {
      "websiteName": getEnv("APP_NAME"),
      "pageTitle": "Todo-List: Add an item",
  }
  render(view, fields, getEnv("DIR_PARTIALS"))

proc store*(req: Request): void =
  if req.params["text"] == "":
    type emptyParam = object of Exception
    raise newException(emptyParam, "No input")
  let query = sql"""
  INSERT INTO todo_list (text)
  VALUES (?)
  """
  let db = open(getEnv("DB_DATABASE"), "", "", "")
  db.exec(query, req.params["text"])
  db.close()

proc destroy*(id: string): void =
  let query = sql"""
  DELETE FROM todo_list
  WHERE ID = ?
  """
  let db = open(getEnv("DB_DATABASE"), "", "", "")
  db.exec(query, id)
  db.close()
